\section{Symmetric monoidal $\infty$-categories and module objects}
\subsection{(Un-)straightening}
\begin{mrem}
	I added things from \cite{barwick-fib}, \cite{ferdi} and \cite{harpaz-cart} to this subsection.
\end{mrem}
\begin{defn}
	Let $p\mc \ecat \to \ccat$ be a functor of $\infty$-categories.
	A morphism $f\mc x\to y$ in $\ecat$ is \emph{$p$-cocartesian} if for all $z\in \ecat$, the following diagram is a pullback diagram in the $\infty$-category of anima\footnote{Formally known as ``spaces''.}:
	\[
		\begin{tikzcd}
			\hom_{\ecat}(y,z)
			\ar{r}[above]{f^{\ast}}
			\ar{d}[left]{p}
		&
		\hom_{\ecat}(x,z)
		\ar{d}[right]{p}
		\\
		\hom_{\ccat}(p(y),p(z))
		\ar{r}[below]{p(f^{\ast})}
		&
		\hom_{\ccat}(p(x),p(z))
		\end{tikzcd}
	\]
	
\end{defn}

\begin{rem}
	\leavevmode
	\begin{enumerate}
		\item 
			One has to be careful with the precise meaning of  \enquote{the pullback along $f$}, since precomposition for $\infty$-categories is only well-defined up to some (\coms contractible?\come) choice.
			I guess one also has to be a bit cautios with using a ``pullback diagram in $\ancat$'', since sometimes $\hom$-functors are used to define limits (which in turn sometimes need the straightening-construction below for their construction).
			I don't know what the ``smart'' order of doing all this is, but this problem can probably also be avoided if we use the model category structure on $\ancat$ to define ``homotopy pullbacks'' in a more classical sense.
		\item
			Informally, that the above diagram is a pullback means that there is a unique solution in the following lifting problem:
			\begin{equation}
				\label{3:informal-cocart}
				\begin{tikzcd}
					x
					\ar{rr}
					\ar{rd}[above right]{f}
					\ar[mapsto]{dd}
					&
					&
					z
					\ar[mapsto]{dd}
					\\
					&
					y
					\ar[dashed]{ur}[above left]{\exists!}
					&
					\\
					p(x)
					\ar{rr}
					\ar{rd}
					&
					&
					p(z)
					\\
					&
					p(y)
					\ar{ur}
					\ar[mapsfrom, crossing over]{uu}
					&
				\end{tikzcd}
			\end{equation}
				Here, we mean ``unique'' as in ``unique up to contractible choice''.
				So to put it in slightly different words, the space of lifts of $p(y) \to p(z)$ is contractible.
				\coms Is this an equivalent conditions, or do we need to choose the contractions \enquote{in a coherent way} too?\come
	
	\end{enumerate}
	
\end{rem}

\begin{defn}
	Let $p\mc \ecat \to \ccat$ be a functor of $\infty$-categories.
	Let $\arcat(\ecat)$ be the \emph{arrow category} of $\ecat$, and denote by $p\mhyp\cocart$ the full subcategory spanned by the $p$-cocartesian fibrations.
	The functor $p$ is called \emph{cocartesian} if the induced map $p\mhyp\cocart\to P$ in the following fiber product of $\infty$-categories is an equivalence:
		\[
			\begin{tikzcd}
				p\mhyp\cocart
				\ar[bend left = 20]{rrd}[above right]{p}
				\ar[bend right = 20]{ddr}[below left]{s}
				\ar[dashed]{dr}[above right]{\sim}
				&
				&
				\\
				&
				P
				\ar{r}
				\ar{d}
				\ar[phantom]{dr}{\lrcorner}
				&
				\arcat(\ccat)
				\ar{d}[right]{s}
				\\
				&
				\ecat
				\ar{r}[below]{p}
				&
				\ccat
			\end{tikzcd}
		\]
\end{defn}

\begin{rem}
	The functor $p\mhyp\cocart\to P$ in the above diagram is actually always fully faithful (\cite{ferdi}*{I.24b}).
\end{rem}

\begin{mrem}
	Again, an informal way of stating the condition to be a cocartesian fibration is the following:
	For every object $x$ of $\ecat$ and every morphism $f'\mc p(x) \to y'$ in $\ccat$, there is a $p$-cocartesian map $f\mc x \to y$ in $\ecat$ such that $p(f) = f'$, or to put in words: morphisms in $\ccat$ where the source is in the image of $p$ can be lifted to $p$-cocartesian edges of $\ecat$.
\end{mrem}

\begin{mexample}
	\label{3:arrowcat-co-cartesian}
	Let $\ccat$ be a $\infty$-category with arrow category $\arcat(\ccat) \defined \funcat(\Delta^1,\ccat)$, and consider the target morphism $t\mc \arcat(\ccat)\to \ccat$.
	Let $f\mc x \to y$ and $f'\mc x' \to y'$ be objects of $\arcat(\ccat)$, and let $\sigma$ be a morphism between $f$ and $f'$, i.e. a commutative square of the form 
	\[
		\begin{tikzcd}
			x
			\ar{d}[left]{f}
			\ar{r}
			\ar[phantom]{rd}{\sigma}
			&
			x'
			\ar{d}[right]{f'}
			\\
			y
			\ar{r}
			&
			y'
		\end{tikzcd}
	\]
	Then $\sigma$ is a $t$-cocartesian edge if and only if $\sigma_0 \mc x \to x'$ is an equivalence in $\ccat$.
	This implies that $t$ is a cocartesian: for every object $f\mc x\to y$ of $\arcat(\ccat)$ and morphisms $y \to y'$ in $\ccat$, a $t$-cocartesian lift is given by 
	\[
		\begin{tikzcd}
			x
			\ar{d}[left]{f}
			\ar[equal]{r}
			&
			x
			\ar{d}
			\\
			y 
			\ar{r}
			&
			y'
		\end{tikzcd}
	\]
	Furthermore, the $t$-cartesian edges in $\arcat(\ccat)$ are given precisely by the pullback square, so the functor $t$ is a cartesian fibration if and only if $\ccat$ admits pullbacks (check \cite{ferdi}*{II.25} for proofs).
\end{mexample}

\begin{defn}
	Let $\ccat$ be an $\infty$-category.
	We define the $\infty$-category $\cocart(C)$ to be the subcategory of $\catinfty/\cc$ with
	\begin{itemize}
		\item 
			generating vertices given by functors $p\mc \ecat \to \ccat$ that are cocartesian; and 
		\item
			generating edges given by pairs $(\ecat,p)$ and $(\ecat',p')$, and functors $F\mc \ecat \to \ecat'$ such that the diagram 
			\[
				\begin{tikzcd}
					\ecat
					\ar{rr}[above]{F}
					\ar{rd}[below left]{p}
					&
					&
					\ecat'\ar{ld}[below right]{p'}
					\\
					&
					\ccat
					&
				\end{tikzcd}
			\]
			commutes and that $p$-cocartesian maps in $\ecat$ get mapped under $F$ to $p'$-cocartesian maps in $\ecat'$.

	\end{itemize}
	\glsadd{cocart}
\end{defn}

\begin{theorem}
	Let $\ccat$ be an $\infty$-category.
	Then there are natural equivalences 
	\[
		\mathrm{St}
		\mc 
		\cocart(\ccat)
		\lrisomorphism
		\funcat(\ccat,\catinfty)
		\mcc
		\mathrm{Un},
	\]
	called the \emph{straightening} respectively the \emph{unstraightening} functor.
	\glsadd{straight}
	\glsadd{unstraight}
\end{theorem}


\begin{example}
	\leavevmode
	\begin{enumerate}
	\item
	Let $C \in \catcat_1$ be a 1-category.
	Then the above equivalence restricts to an equivalence 
	\[
		\cocart_1(C)
		\lrisomorphism
		\funcat(C,\catcat)
	\]
	of $(2,1)$-categories.
	This is called the \emph{Grothendieck construction}.
	We describe how this equivalence looks on objects:
	\begin{itemize}
		\item
		For the straightening functor, let $p\mc E \to C$ be a cocartesian fibration.
		We associate to it a functor $F\mc C \to \catcat_1$, that maps an object $\overline{x}\in C$ to its fiber $p^{-1}(\overline{x})\in \catcat_1$; to a morphism $\overline{f}\mc \overline{x}\to \overline{y}$, we associate a functor 
		\[
			p^{-1}(\overline{x}) \to p^{-1}(\overline{y}),
		\]
		that maps an object $x\in p^{-1}$ to some $y\in p^{-1}(\overline{y})$, such that there is a morphism $x\to y$ in $E$ that is a $p$-cocartesian lift of $\overline{f}$.
		\item
			Turning to the unstraightening functor, let $F\mc C \to \catcat_1$ be a functor.
			We associate to it the category $E$, which has objects given by pairs $(\overline{x},x)$ such that $\overline{x}\in C$ and $x\in F(\overline{x})$ (note that $F(\overline{x})$ is a category), and morphisms given by
			\[
				\hom_{E}
				((\overline{x},x),(\overline{y},y))
				\defined 	
				\lset
				(\overline{f},f)
				\ssp
				\begin{array}{l}
					\text{$\overline{f}\mc \overline{x}\to \overline{y}$ a morphism in $C$};
					\\
					\text{$f\mc F(\overline{f})(x)\to y$ a morphism in $F(\overline{y})$}
				\end{array}
				\rset.
			\]
			The category $E$ is sometimes denoted by $\int_C F$.
	\end{itemize}
	\item
		As a concrete example of this, consider the \emph{category of modules} $\modcat$, that is construct as follows:
		\begin{itemize}
			\item
				Objects are given by pairs $(R,M)$, where $R$ is a ring and $M$ is an $R$-module.
			\item
				For pairs $(R,M)$ and $(S,N)$ in $\modcat$, we set 
				\[
					\hom_{\modcat}((R,M),(S,N))
					\defined 
					\lset 
					(\pphi,f)
					\ssp 
					\begin{array}{l}
						\text{$\pphi\mc R \to S$ is a ring map;}
						\\
						\text{$f\mc M \to N$ is $R$-linear} 
					\end{array}
					\rset.
				\]
		\end{itemize}

		Now the forgetful functor
	\[
		p\mc \modcat \to \ringcat,~(M,R) \mapsto R
	\]
	is a cocartesian fibration:
	If $R\to S$ is a ring map and $M$ an $R$-module, then a $p$-cocartesian lift of $f$ is given by $(R,M) \to (S,S\tensor_R M)$, where the $R$-linear map in the second factor is given by $m \mapsto 1 \tensor m$ --- indeed, if we are supplied with any other tuple $(S',N')$ in $\modcat$, a ring map $g\mc S \to S'$ and a map $(h,\alpha)\mc (R,M) \to (S',N')$ in $\modcat$ as in \eqref{3:informal-cocart}, then the map 
	\[
		S\tensor_R M 
		\to 
		N'
		,
		~
		m\tensor s 
		\mapsto 
		g(s')\alpha(m)
	\]
	is the unique $S$-linear lift that makes \eqref{3:informal-cocart} commute.
	Under the Gro\-then\-dieck construction, this corresponds to the functor 
	$
		\ringcat 
		\to 
		\catcat_1
	$
	that maps a ring $R$ to the category $\modcat(R)$ of $R$-modules, and a ring map $f\mc R \to S$ to the base change functor $\modcat(R) \to \modcat(S)$. 
	\end{enumerate}
\end{example}

%\begin{mrem}
%	Another application of the straightening/unstraightening equivalence is the construction of a $\hom$-functor $\ccat\op \times \ccat \to \ancat$ for any $\infty$-category $\ccat$.
%	This is done in \cite{htt}*{5.2.1}, and I will attempt a brief outline below:
%	\begin{enumerate}
%		\item 
%			Let $\ccat$ be an $\infty$-category.
%			Associated to it its \emph{twisted arrow category} $\twarr(\ccat)$, which is roughly constructed as follows:
%			We consider the simplicial set $Q \mc \Delta \to \Delta $, which is given by $[n] \mapsto [2n+1]$ on objects, on morphisms, it `` splits $[2n+1]$ in the middle and then applies the morphism to the first half, and the opposite morphism to the second half''.\footnote{A more precise way of saying this is that $Q$ is given by the functor of partially ordered sets $I \mapsto I \star I\op$.}
%			Now $\twarr(\ccat)$ is the simplicial set given by $[n] \mapsto \ccat(Q([n]))$.	
%			Objects of $\twarr(\ccat)$ are morphisms in $\ccat$; morphisms in $\twarr(\ccat)$ are given by commutative diagrams in $\ccat$ of the form 
%			\[
%				\begin{tikzcd}
%					C
%					\ar{r}
%					\ar{d}
%					&
%					D
%					\\
%					C'
%					\ar{r}
%					&
%					D'
%					\ar{u}
%				\end{tikzcd}
%			\]
%			\item 
%				One then shows that the canonical map $\twarr(\ccat) \to \ccat\op \times \ccat$ is a right a \emph{right-fibration}, i.e. that it has the lifting property with respect to all horn inclusions $\Lambda^n_u \hookrightarrow \Delta^n $ for $0<i\leq n$ (this needs combinatorics).
%				(\coms come back to this after having spent more time with \cite{barwick-fib}.\come)
%	\end{enumerate}
%\end{mrem}

\subsection{Symmetric monoidal $\infty$-categories}
\begin{prop}
	Denote by $S^1 \defined \Delta^1 / \partial \Delta^1$ the simplicial circle.
	Then the suspension in $H(Z)_{\ast}$ is given by $ (-)\wedge S^1$.
\end{prop}

\begin{defn}
	\leavevmode
	\begin{enumerate}
		\item
		A commutative monoid is a pair $(M,\cdot)$, where $M$ is a set and $\cdot \mc M \times M \to M$ is a map satisfying the following three axioms: 
		\begin{itemize}
			\item 
				Commutativity: for all $x,y\in M$, it holds that $x\cdot y = y\cdot y$;
			\item
				Associativity: for all $x,y,z\in M$, it holds that $(x\cdot y)\cdot z = x\cdot (y\cdot z)$;
			\item
			Existence of a unit: there is a $e\in M$ such that $e\cdot x = x$ holds for all $x\in M$. 
		\end{itemize}
	\item
		A \emph{symmetric monoidal $1$-category} is a tuple $(M,\tensor,\beta,\gamma)$, where $M$ is a $1$-category, $\tensor$ is a functor $M\times M \to M$; $\beta$ and $\gamma$ are natural transformations, where: 
		\begin{itemize}
			\item 
				$\beta$ encodes commutativity, i.e. $\beta_{x,y}\mc x\tensor y \isomorphism y\tensor x$;
				\item
					$\gamma$ encodes associativity, i.e. $\gamma_{xyz}\mc (x\tensor y)\tensor z \isomorphism x\tensor (y\tensor z)$.
		\end{itemize}
		The natural transformations are supposed to satisfy certain compatibilities (\coms c.f.\come \cite{kerodon}).
		There is a (correct) notion of functors between symmetric mo\-noi\-dal categories, which leads to the 1-category $\symmodcat_1$ of symmetric monoidal $1$-categories.
	\end{enumerate}
\end{defn}
\begin{example}
	Let $R$ be a ring, then the category $\modcat(R)$ together with the tensor product $-\tensor_R -$ is a symmetric monoidal category.
\end{example}
\begin{numtext}
	We now want to extend this notion to $\infty$-categories.
	The problem is that we need a good way to encode ``higher compatibilities''.
\end{numtext}
\begin{defn}
	We define a 1-category $\fincat$ of \emph{finite pointed sets}, which has
	\begin{itemize}
		\item 
			objects given by unordered sets $\brac{n} \defined \lset \ast,1,\ldots,n\rset$;
		\item
			morphisms given by maps of pointed sets.
	\end{itemize}

	For $1\leq i \leq n$, we define the map $\rho_i^n$ as: 
	\[
		\rho_i^n\mc
		\brac{n}
		\to
		\brac{1}
		,~
			i\mapsto 1,~
			i\neq j
			\mapsto 
			\ast
	\]
\end{defn}
\begin{rem}
	We write $\brac{n}^{\circ} \defined \lset 1,\ldots, n\rset \sse \brac{n}$.
	Using this notation, we want to think of maps $\brac{m}\to\brac{n}$ in $\fincat$ as partially defined maps of sets that are defined on $\brac{m}^{\circ}\to \brac{n}^{\circ}$.
\end{rem}

\begin{construction}
	\leavevmode
	\begin{enumerate}
		\item
	Let $(M,\tensor,\beta,\gamma)$ be a symmetric monodial 1-category.
	We define a functor 
	\[
		X_M\mc \fincat \to \catcat_1,
\]
	that is given on objects by $\brac{n} \mapsto M^n$; for a morphism $\alpha \mc \brac{m} \to \brac{n}$, the functor $X_m(\alpha)$ is given on objects by 
	\[
		M^m
		\to
		M^n
		,
		~
		(x_i)_{i=1}^m
		\mapsto 
		\Big(\bigotimes_{j\in \alpha^{-1}(i)} x_j\Big)_{i=1}^n
	\]
	\item
		Let $(M^{\tensor} \to \fincat)\in \cocart_1(\fincat)$ be the unstraightening of $X_M$.
		Concretely, the category $M^{\tensor}$ can be described as follows:
		\begin{itemize}
			\item
				Objects in $M^{\tensor}$ are given by tuples $(\brac{n},x_1,\ldots,x_n)$, where $\brac{n}$ in $\fincat$ is a finite pointed set and $x_1,\ldots,x_n$ are objects on $M$.
			\item
				Given two objects $(\brac{m},x_1,\ldots,x_m)$ and $(\brac{n},y_1,\ldots,y_n)$ of $M^{\tensor}$, the set of morphisms between them is given by 
				\[
					\lset
					(\alpha,f_1,\ldots,f_n)
					\ssp 
					\begin{array}{l}
						\text{
						$	
						\alpha 
						\mc 
						\brac{m}
						\to 
						\brac{n}
						$
						is a morphism in $\fincat$,
					}
					\\
					\text{
						$f_i\mc \bigotimes_{j\in \alpha^{-1}(i)} x_j
						\to 
						y_i
						$
					is a morphism in $M$}
				\end{array}
				\rset
			\]		
		\end{itemize}
		\item
		The construction 
		\[
		X_{(-)}\mc 
		\symmodcat_1
		\to 
		\funcat(\fincat,\catcat_1)
		\]
		is a fully faithful functor, whose essenital image is given by all these functors $X\mc \fincat\to \catcat_1$ for which for all $n\geq 0$, the maps $\rho_i^n$ assemble to isomorphisms 
		\[
			\left(\rho_i^n\right)_{i=1}^n
			\mc 
			X_n 
			\isomorphism
			\prod_{i=1}^n
			X_1.
		\]
	\end{enumerate}
\end{construction}
\begin{defn}
	The datum of a \emph{symmetric monoidal $\infty$-category} is given by a functor $F \mc \fincat \to \catcat_{\infty}$ such that for all $n\geq 0$, the maps $\rho^n_i$ assemble into isomorphisms
	\[
		\left(\rho_i^n\right)_{1\leq i \leq n}\mc
		F(n)
		\isomorphism
		\prod_{i=1}^n F(1)
	\]
	We denote by $\symmodinfty \sse \funcat(\fincat,\catinfty)$ the full subcategory spanned by the symmetric monoidal $\infty$-categories.
	If $F\mc \fincat \to \catinfty$ is a symmetric monoidal $\infty$-category, we call $\mcat \defined F(1)$ the underlying $\infty$-category of $F$, and write $X_{\mcat} \defined F$.
	Using the $\infty$-Grothedieck construction, we also obtain a functor $(\mcat^{\tensor} \to \fincat)\in \cocart(\fincat)$ for every symmetric monoidal $\infty$-category $X_{\mcat}$.
	Actually, we call any of the $X_{\mcat}$, $\mcat^{\tensor}$ and $\mcat$ a symmetric monoidal $\infty$-category.
\end{defn}

\begin{example}
	\leavevmode
	\begin{enumerate}
		\item
			The 1-category $\symmodcat_1$ is a full subcategory of $\symmodinfty$.
		\item
			Given an $\infty$-category with finite products, we can equip it with the \emph{cartesian symmetric monoidal structure}.
	\end{enumerate}
\end{example}
\subsection{Operads}

\begin{defn}
	A \emph{1-operad} $\oo$ consists of the following data:
	\begin{itemize}
		\item
			A class object (which we also denote by $\oo$);
		\item
			For all finite sets $I$ and objects $(a_i)_{i\in I},b\in \oo$, a set $\mulmaps_{\oo}((a_i)_{i\in I}, b)$, which we call the set of \emph{multimaps} between $(a_i)_{i \in I}$ and $b$;
		\item
			For all maps of finite sets $\alpha \mc I \to J$ and objects $(a_i)_{i\in I}, (b_j)_{j\in J}, c\in \oo$ a map 
			\[
				\mulmaps_{\oo}((b_j)_{j\in J},c)
				\times
				\prod_{j\in J}
				\mulmaps_{\oo}((a_i)_{i\in \alpha^{-1}(j)},b_j)
				\to 
				\mulmaps_{\oo}((a_i)_{i\in I}, c)
			\]
			which is called the \emph{composition map}.
	\end{itemize}
	\glsadd{multimaps}
	This data is required to be suitably associative and to have identity maps.
	We can then construct a category $\opacat_1$ of $1$-operads.
	We also have, for any two 1-operads $\oo,\oo'$, a functor category $\funcat(\oo,\oo')\in \catcat_1$.
\end{defn}

\begin{example}
	\label{iii:operad-basic-ex}
	\leavevmode
	\begin{enumerate}
	\item
	Let $(M,\tensor,\beta,\gamma)$ be a symmetric monoidal 1-category.
	Then we can define a 1-operad $\widetilde{M}$, which has the same objects as $M$; for $(x_i),y\in \widetilde{M}$, we set 
	\[
		\mulmaps_{\widetilde{M}}
		((x_i)_I,y)
		\defined 
		\hom_{M}(\tensor_I x_i, y).
	\]
	This gives a (non-full!) inclusion $\symmodcat_1 \sse \opacat_1$.
	\item
		The operad $\comop$ has a single object $a$, and multimaps are given by one-point sets:
		\[
			\mulmaps_{\comop}((a)_I,a)
			\defined 
			\lset
			\ast
			\rset.
		\]
	\item
		The operad $\comop^{[1]}$ has two objects $a,b$ and multimaps are defined as follows:
		\[
			\mulmaps_{\comop^{[1]}}
			((x_i)_I,y)
			\defined 
			\begin{cases}
				\lset \ast \rset
				&
				\text{if $x_i=a$ for all $i\in I$ and $y=a$;}
				\\
				\lset \ast \rset 
				&
				\text{if $y = b$;}
				\\
				\emptyset
				&
				\text{otherwise.}
			\end{cases}
		\]
	\item
	The operad $\commodop$ has two objects $a,m$, and multimaps are defined as follows:
	\[
		\mulmaps_{\commodop}((x_i)_I,y)
		\defined 
			\begin{cases}
				\lset \ast \rset
				&
				\text{if $x_i=a$ for all $i\in I$ and $y=m$;}
				\\
				\lset \ast \rset 
				&
				\text{if $x_i = m$ for a unique $i\in $ and $y=m$;}
				\\
				\emptyset
				&
				\text{otherwise.}
			\end{cases}
	\]
\end{enumerate}
	We also have various maps between the last three examples:
	\begin{itemize}
		\item
			Two maps $F_1,F_2\mc \comop \to \comop^{[1]}$, that send the single point of $\comop$ either to the element $a$ or the element $b$ of $\comop{[1]}$.
		\item
			A map $G\mc \comop \to \commodop$, which sends the single point of $\comop$ to the element $a$ of $\commodop$.
		\item
			A map $H\mc \commodop \to \comop^{[1]}$, given by $a\mapsto a$ and $m \mapsto b$.

	\end{itemize}
\end{example}

\begin{defn}
	Let $\oo \in \opacat_1$ be a 1-operad and $M \in \symmodcat_1$ a symmetric monoidal 1-category.
	An \emph{$\oo$-algebra in $M$} is a map of 1-operads $\oo \to \widetilde{M}$.
	We set $\alg_{\oo}(M) \defined \funcat(\oo,\widetilde{M})$ (\coms do we want this or $\hom_{\opacat_1}(\oo,\widetilde{M})$? \come).
\end{defn}

\begin{rem}
	Let $\oo \to \oo'$ be a map of operads, and $M$ a symmetric monoidal 1-category.
	Then we get a restriction functor $\alg_{\oo'}(M) \to \alg_{\oo}(M)$.
\end{rem}

\begin{example}
	\label{iii:ab-example-1}
	Let $M \defined (\abcat,\tensor)$ be the category of abelian groups with the ordinary tensor product.
	Then we have:
	\begin{itemize}
		\item
			$\alg_{\comop}(\abcat) \cong \cringcat$;
		\item
			$\alg_{\comop^{[1]}}(\abcat) \cong \lset \pphi \mc A \to B \ssp A,B\in \cringcat\rset = \arcat(\cringcat)$;
		\item 
			$\alg_{\commodop}(\abcat) \cong \lset (R,M) \ssp R\in \cringcat, M \in \modcat(R) \rset = \modcat$.
	\end{itemize}
	We can also describe the functors that are induced by the various maps specified in \cref{iii:operad-basic-ex}:
	\begin{itemize}
		\item
			The functor $F_1^{\ast},F_2^{\ast}$ associate to ring map its source respectively its target.
		\item 
			The functor $G^{\ast}$ associates to a pair $(A,M) \in \modcat$ the underlying ring $A \in \cringcat$.
		\item
			The functor $H^{\ast}$ associates to a ring map $\pphi \mc R\to S$ the pair $(R,S) \in \modcat$, where we regard $S$ as an $R$-module via $\pphi$.
	\end{itemize}
\end{example}

\begin{construction}
	\leavevmode
	\begin{enumerate}
		\item 
			Let $\oo\in \opacat_1$ be a 1-operad.
			We define a 1-category $\oo^{\tensor}$ that has objects given by tuples of the form 
			\[
				\left(
					\brac{n},
					a_1,\ldots,a_n
				\right)
			\]
			for $\brac{n}\in \fincat$ und $a_1,\ldots,a_n \in \oo$; morphisms are given by 
			\[
				\hom_{\oo^{\tensor}}((\brac{m},(a_i)_{i=1}^m,(\brac{n},(b_j)_{j=1}^n))
				\defined 
				\lset
				(\alpha,(f_k)_{k=1}^{n})
				\ssp
				\begin{array}{l}
					\text{$\alpha \mc \brac{m} \to \brac{n}$ in $\fincat$,}
					\\
					\text{$f_i \in \mulmaps_{\oo}((a_j)_{j\in \alpha^{-1}(i)},b_i)$}
				\end{array}
				\rset
			\]
		This construction comes with a natural functor 
		\begin{align*}
			\oo^{\tensor}
			&
			\longrightarrow
			\fincat
			\\
			(\brac{n},a_1,\ldots,a_n)
			&
			\longmapsto
			\brac{n}
			\\
			(\alpha,f_1,\ldots,f_n)
			&
			\longmapsto
			\alpha
		\end{align*}
		\item
			This map is in general not cocartesian, but we can single out a set of morphisms that always have cocartesian lifts:
			For that, we say that a map $\brac{m} \to \brac{n}$ is \emph{inert} if $\abs{\alpha^{-1}(i)} = 1$ holds for all $i \in \brac{n}^{\circ}$.
			Inert maps always have cocartesian lifts --- given $(\brac{m},a_1,\ldots,a_m)\in \oo^{\tensor}$ and $\alpha\mc \brac{m} \to \brac{n}$ an inert map in $\fincat$, such a cocartesian lift is given by 
			\[
				(\alpha,\id,\ldots,\id)
				\mc
				(\brac{m},a_1,\ldots,a_m)
				\to 
				(\brac{n},a_{\alpha^{-1}(1)},\ldots a_{\alpha^{-1}(n)}).
			\]
		\item
			Given 1-operads $\oo_1,\oo_2\in \opacat_1$, we have a fully faithful inclusion 
			\[
				\funcat(\oo_1,\oo_2)
				\sse
				\funcat_{\fincat}(\oo_1^{\tensor},\oo_2^{\tensor})
			\]
			which has essential image given by those functors that preserve cocartesian maps lying over inert maps in $\fincat$.
	\end{enumerate}
\end{construction}

\begin{defn}
	Let $\oo\in \opacat_1$ be a 1-operad and $M \in \symmodinfty$ be a symmetric monoidal $\infty$-category.
	Then we define the category $\alg_{\oo}(M)$ of \emph{$\oo$-algebra objects in $M$} as the full subcategory of $\funcat_{\fincat}(\oo^{\tensor},M^{\tensor})$ spanned by those functors $(\oo^{\tensor},p_{\oo}) \to (M^{\tensor},p_m)$ that send $p_{\oo}$-cocartesian maps in $\oo^{\tensor}$ that lie over inert maps in $\fincat$ to $p_M$-cocartesian maps in $M$.
\end{defn}

\begin{rem}
	\leavevmode
	\begin{enumerate}
		\item
			This definition extends the definition for algebra objects in a symmetric monoidal 1-category.
		\item
			Again, we have contravariant functoriality of $\alg_{\oo}(M)$ with respect to maps of 1-operads.
	\end{enumerate}
\end{rem}

\begin{defn}
	Let $\mcat \in \symmodinfty$ be a symmetric monoidal $\infty$-category.
	\begin{enumerate}
		\item
			The category of \emph{commutative algebras} in $\mcat$ is defined as 
			\[
				\calg(\mcat)
				\defined 
				\alg_{\comop}(\mcat).
			\]
		\item 
			Let $A \in \calg(\mcat)$ be a commutative algebra in $\mcat$.
			Then the category of \emph{$A$-algebras} is defined as the following pullback:
			\[
				\begin{tikzcd}
					\calg(A)
				\ar{r}
				\ar{d}
				\ar[phantom]{rd}{\lrcorner}
				&
				\alg_{\comop^{[1]}}(\mcat)
				\ar{d}[right]{a^{\ast}}
				\\
				\left[0\right]
				\ar{r}[below]{A}
				&
				\calg(\mcat)
				\end{tikzcd}
			\]
		\item
			Let $A\in \calg(\mcat)$ be a commutative algebra in $\mcat$.
			Then the category of \emph{A-modules} is defined as the following pullback:
			\[
				\begin{tikzcd}
					\mmodcat(A)
				\ar{r}
				\ar{d}
				\ar[phantom]{rd}{\lrcorner}
				&
				\alg_{\commodop}(\mcat)
				\ar{d}
				\\
				\left[0\right]
				\ar{r}[below]{A}
				&
				\calg(\mcat)
				\end{tikzcd}
			\]
	\end{enumerate}
\end{defn}

\begin{mexample}
	Let again $\mcat = \widetilde{\abcat}$, as in \cref{iii:ab-example-1}.
	We have already seen that $\calg(\widetilde{\abcat}) = \cringcat$, and that $\alg_{\comop^{[1]}}(\widetilde{\abcat}) = \arcat(\cringcat)$.
	Let $A$ be a ring.
	Then the map $a^{\ast}$ associates to a ring map its codomain:
	\begin{align*}	
		a^{\ast}
		\mc 
		\arcat(\cringcat)
		&
		\longrightarrow
		\cringcat
		\\
		\left(R\to S\right)
		&
		\longmapsto 
		R,
	\end{align*}
	and we can identify $\calg(A)$ with the the fiber of $A$ under $a^{\ast}$.
	So we have 
	\[
		\calg(A)
		=
		\lset
		A\to R \ssp R \in \cringcat
		\rset
		\sse
		\arcat(\cringcat),
	\]
	which agrees with the usual notion of commutative $A$-algebras.
	Similarly, we get that our \enquote{new} notion of modules over a commutative ring agrees with the usual one.
\end{mexample}
