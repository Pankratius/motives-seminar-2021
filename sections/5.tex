\section{Algebraic stacks}
\begin{litrem}
	\cite{bejleri}, \cite{olsson}
\end{litrem}
\subsection{Algebraic spaces}
\subsubsection{Étale equivalence relations}
\begin{mdefn}
	Let $S$ be a scheme and $X \to S$ an $S$-scheme.
	An \emph{equivalence relation} on $X$ is a monomorphism $\iota = (\iota_1,\iota_2) \mc R \to X \times_S X$ of $S$-schemes, such that for every $T \to S$, the subset 
	\[
		R(T) 
		\sse
		X(T) \times_S X(T)
	\]
	is an equivalence relation in the ordinary sense.
	We say that such an equivalence relation is an \emph{étale/fppf equivalence relation} if the compositions $\projection_i \circ j \mc R \to U$ are étale/fppf maps for $i=1,2$.
\end{mdefn}
	
\begin{mexample}
	If $\rho \mc G\times X \to X$ is a free action of a $X$-group scheme $G$, then $R_G \defined \id \times \rho$ defines an étale equivalence relation.
\end{mexample}

\begin{mconstruction}
	Let $X \to S$ be a fppf-scheme and let $R$ be an equivalence relation on $X$.
	Let $\mathcal{T} \in \lset \text{fppf, étale,...} \rset$.
	The \emph{quotient sheaf} associated to $R$ is the sheafification of the $\tau$-presheaf $T \mapsto X(T)/{\sim}_T$, where $\sim_T$ is the equivalence relation on $X(T)$ induced by $R$.
	It is denoted by $X/R$.
	The projection maps $X(T) \twoheadrightarrow X(T)/{\sim}_T$ induce a map $h_X \to X/R$ in $\sheafcat_{\tau}(S)$.
	If $R \in \sheafcat_{\tau}(S)$, then this map turns the diagram 
	\[
		\begin{tikzcd}[column sep = small, cramped]
			h_R
			\ar[shift left = 1]{r}
			\ar[shift right = 1]{r}
			&
			h_X
			\ar{r}
			&
			X/R
		\end{tikzcd}
	\]
	into a coequalizer diagram in $\sheafcat_{\tau}(S)$.
	If $X/R$ is representable by a $\tau$-scheme, then this says precisely that the quotient is a \emph{categorical quotient} in the sense of \cite{stacks}*{\stackstag{048I}}.
\end{mconstruction}

\begin{mexample}
	Let $G \to X$ be a finite flat group scheme, and $\rho \mc X \times G \to X$ a group action.
	Then the map $\id \times \rho \mc X \times X \times G \to X \times X$ is a fppf-equivalence relation.
	If $\rho$ is free, then the categorical quotient exists (and coincides with the quotient by this equivalence relation).
	If $X = \spec(A)$ is affine, then it is given by $\spec(A^G)$, where 
	\[
		A^G = \lset a \in A \ssp \rho^{\#}(a) = a \tensor 1 \rset
	\]
	for the map $\rho^{\#} \mc A \to A \tensor H$ (where $G = \spec(H)$).
	\coms not sure if this is all left-right-correct \come
\end{mexample}

\begin{example}
	Let $k = \cc$, and set $ S \defined \spec_k$ as well as $X \defined \affa^1_k$.
	Then the map 
	\[
		R
		\defined 
		\affa_k^1 
		\amalg	
		\left( \affa_k^1 \setminus \lset 0 \rset \right) 
		\to 
		X \times_S X
	\]
	which is given in the first component by $ x \mapsto (x,x)$ and in the second by $y \mapsto (y,-y)$ defines an étale equivalence relation on $X$.
	The quotient sheaf $\shf \defined X/R$ fits into the following commutative diagram of sheaves on $\sheafcat_{\mathrm{\acute{e}t}}(S)$:
		\[
			\begin{tikzcd}
			R
			\ar{r}[above]{j}
			\ar{d}
			\ar[phantom]{rd}{\lrcorner}
			&
			X \times_S X
			\ar{d}
			\\
			\shf
			\ar{r}[below]{\Delta}
			&
			\shf \times_S \shf
			\end{tikzcd}
		\]
		The image of $j$ is not open in $\affa_k^2$: otherwise, there would be a nontrivial polynomial $f\in \cc[x,y]$ that vanishes outside of it.
		But in that case, the polyonomial $(x+y)(x-y)f$ vanishes in all of $\affa_k^2$, and hence is trivial, which would imply that $f$ is trivial.
		This implies that $\shf$ can't be represented by a scheme, as for schemes the map $\Delta$ is an immersion, which would imply that $j$ is open.
\end{example}

\subsubsection{Algebraic spaces}
\begin{defn}
	An \emph{algebraic space} is an étale sheaf $\sha \in \shetcat_k$ satisfying the following two conditions:
	\begin{enumerate}
		\item
			\emph{Local representability}:
			There exists a scheme $U$ and a map of sheaves $U \to \sha$ such that for all schemes V and maps $V \to \sha$ the fiber product $U \times_{\sha} V$ is representable by a scheme and the projection map $V \times_{\sha} U$ is induced by an étale surjective map of schemes.
		\item
			\emph{Quasi-seperatedness}: 
			For any $U \to \sha$ as above, the map of schemes that induces $U\times_{\sha} U \to U \times U$ is quasi-compact.
	\end{enumerate}
\end{defn}

\subsection{Stacks in general}

\begin{defn}
	Let $\mathcal{T}$ be a Grothendieck topology on $\schemecat_k$.
	A \emph{stack} for the $\mathcal{T}$-topology is a presheaf of groupoids 
	$
		\stackx 
		\mc 
		\schemecat_k
		\to 
		\groupoidcat
	$
	that satisfies $\tcat$-descent. 
\end{defn}

\begin{rem}
	We have to interpret the notion of a ``presheaf of groupoids'' in a 2-categorical setting, otherwise we don't have any functoriality for the pullback-assignement 
	\[
		(-)^{\ast} \mc \morcat(\schemecat_k) \to \morcat(\groupoidcat),~f \mapsto \stackx(f)
	\]
	One way around this is to define a prestack to be a cartesian fibration $p \mc \ccat \to \schemecat_k$, such that all morphisms in $\ccat$ get mapped to $p$-cartesian morphisms in $\schemecat_k$ (such a cartesian fibration is called \emph{fibered in groupoids}).
	This enhances our definition above in the sense that the straightening equivalence 
		\[
			\mathrm{St}
			\mc
			\cart(\nerve(\schemecat_k))
			\isomorphism
			\funcat(\nerve(\schemecat_k)\op,\catinfty)
		\]
		maps cartesian fibrations that are fibered in groupoids to functors that factor over $\ancat$.
		We then still have to define what it means for a presheaf $\shf \mc \nerve(\schemecat_k)\op \to \ancat$ to satisfy ``$\tcat$-descent''.
		This is done via the \v{C}ech-nerve.
		(\coms reference that these two agree \come)
\end{rem}


\subsection{Algebraic stacks}

\begin{example}
	Let $X$ be an algebraic space and let $G/k$ be a smooth group scheme which acts on $X$.
	Define $[X/G]$ to be the stack whose (unstraightening) has objects given by triples $(T,\shp,\pi)$, where
	\begin{itemize*}
		\item 
			$T$ is a $k$-scheme;
		\item 
			$\shp$ is a $G_T \defined G \times_S T$-torsor on the big étale site of $T$; and 
		\item 
			$\pi \mc \shp \to X \times _S T $ is a $G_T$-equivariant morphism of sheaves.
	\end{itemize*}
	A morphism $(T',\shp',\pi') \to (T,\shp,\pi)$ is a pair $(f,f^{\flat})$, where $f\mc T' \to T$ is an $S$-morphism of schemes, and $f^{\flat} \mc \shp' \isomorphism f^{\ast}\shp$ is an isomorphism of $G_{T'}$-torsors on $(\schemecat_T')$ such that the induced diagram
	\[
		\begin{tikzcd}
			\shp
			\ar{rr}[above]{f^{\flat}}
			\ar{rd}[below left]{\pi'}
			&
			&
			f^{\ast}\shp
			\ar{ld}[below right]{f^{\ast}\pi}
			\\
			&
			X \times_s T'
			&
		\end{tikzcd}
	\]
	commutes.
	It is in fact an algebraic stack.
	One particular instance of this is the case $X = S$, where $G$ acts trivivally.
	In this case the stack quotient $[S/G]$ is denoted by $\classb G$ and is called the \emph{classifying stack} of $G$. 
\end{example}
\subsubsection{Properties of algebraic stacks}
\begin{defn}
	Let $P$ be an absolute property of a scheme. 
	We say that an algebraic stack $\stackx$ \emph{has $P$} if there is an atlas $U \to \stackx$, such that $U$ has $P$.
\end{defn}

\begin{defn}
	A morphism of stacks $f\mc \stackx \to \stacky$ is \emph{representable}, if for all schemes $X$ and maps $X \to \stacky$, the fiber product $X \times_{\stacky} \stackx$ is representable by a scheme.
\end{defn}

\begin{defn}
	Let $P$ be a property of a morphism of schemes, that is stable under base change along surjective smooth maps.
	Let $f\mc \stackx \to \stacky$ be a morphism of algebraic stacks.
	We say that \emph{$f$ has $P$}, if for some atlas $U \to \stackx$, the morphism $U \times_{\stacky} \stackx \to U$ has $P$.
\end{defn}

\begin{example}
	Examples include open/closed immersions, and finite, affine, proper morphisms.
	In particular, we get the notion of an \emph{open substack}.
\end{example}

\begin{defn}
	Let $P$ be a property of morphisms of schemes $f\mc X \to Y$ such that $f$ has $P$ if and only if there is a commutative square of the form 
	\[
		\begin{tikzcd}
			X'
			\ar{r}[above]{f}
			\ar{d}[left]{p'}
			&
			Y'
			\ar{d}[right]{p}
			\\
			X
			\ar{r}[below]{f}
			&
			Y
		\end{tikzcd}
	\]
	with $p,p'$ smooth such that $f'$ has $P$.
	We say that a morphism $f \mc \stackx \to \stacky$ of algebraic stacks \emph{has $P$} if for some atlases $U \to \stackx$ and $V \to \stacky$, there exists a commtuative diagram of the form
	\[
		\begin{tikzcd}
			U
			\ar{r}[above]{f'}
			\ar{d}
			&
			V
			\ar{d}
			\\
			\stackx
			\ar{r}[below]{f}
			&
			\stacky
		\end{tikzcd}
	\]
	such that $f'$ has $P$.
\end{defn}

\begin{example}
	Examples now include morphisms that are smooth, flat or locally of finite presentation.
\end{example}

\begin{defn}
	Let $\stackx$ be an algebraic stack.
	A \emph{quasi-coherent sheaf} $\shf$ on $\stackx$ consists of the following:
	\begin{itemize*}
		\item
			For all smooth maps $x \mc X \to \stackx$ with $X$ a scheme, a quasi-coherent sheaf $\shf_{X,x}$ on $X$;
		\item 
			For all diagrams of the form
			\[
				\begin{tikzcd}
					V
					\ar{rr}[above]{f}
					\ar{rd}[below left]{v}
					&
					&
					U
					\ar{ld}[below right]{u}
					\\
					&
					\stackx
					&
				\end{tikzcd}
			\]
			with $U,V \to \stackx $ smooth morphisms and $\phi \mc u \circ f \isomorphism v$ an isomorphism, an isomrphism $\theta_{f,\phi} \mc f^{\ast} \shf_{U,u} \isomorphism \shf_{V,v}$ as part of the datum of $\shf$.
			The $\theta_{f,\pphi}$ are required to be ``compatible with composition'' in a suitable sense.
	\end{itemize*}
	A \emph{coherent sheaf} on $\stackx$ is a quasi-coherent sheaf $\shf$, such that all the $\shf_{X,x}$ are coherent sheaves.
\end{defn}

\begin{mrem}
	Again, this definition sweeps away the technical difficulty of defining what we mean with ``compatible with compositions''.
	A 2-categorical way of defining this is as 
	\[
		\qcohcat(\stackx)
		\defined 
		\maps_{\catcat_{/\ccat}^{\mathrm{cart}}}(\stackx, \qcohcat(S)),
	\]
	where we write $\ccat \defined \schemecat_S$.
	This turns out to be equivalent to the category of ``descent data on $\qcohcat(S)$ for $\stackx$'' (c.f. \cite{moduli}*{\modulitag{0052}}).
\end{mrem}
\subsubsection{Totaro's theorem}

\begin{defn}
An algebraic stack $\stackx$ is called a \emph{local quotient stack} if there is a covering $\lset \stacku_i\rset
$ by open substacks such that $\stacku_i \cong [U_i/G_i]$ holds for all $i$, where $G_i$ is an algebraic group acting on a scheme $U_i$.
\end{defn}

\begin{defn}
	Let $\stackx$ be an algebraic stack, $T$ a scheme and $x\mc T \to \stackx$ a morphism.
	Consider the fiber product
	\[
		\begin{tikzcd}
			\aut_{\stackx(T)}(x)
		\ar{r}[above]{}
		\ar{d}[left]{}
		\ar[phantom]{rd}{\lrcorner}
		&
		T
		\ar{d}[right]{}
		\\
		\stackx
		\ar{r}[below]{\Delta}
		&
		\stackx \times \stackx
		\end{tikzcd}
	\]
	If $T = \spec(K)$ for some field $K$, i.e. if $x$ is a closed point of $\stackx$, then we call $\aut_{\stackx(T)}(x)$ the \emph{stabilizer} of $x$.	
	\coms Why is this denoted $\aut$...? \come
\end{defn}


\begin{theorem}
	Let $S = \spec(k)$ for $k$ a field.
	Let $\stackx$ be a normal, locally noetherian algebraic stack whose stbilizers at closed points are affine group schemes.
	Then the following are equivalent:
	\begin{enumerate}
		\item 
			$\stackx$ has the resolution property: Every coherent sheaf on $\stackx$ is a quotient of a vector bundle on $\stackx$;
		\item 
			$\stackx$ is isomorphic to the quotient stack of some quasi-affine scheme by an action of $\genlin_n$ for some $n$.
	\end{enumerate}
	If moreover $\stackx$ is of finite type over $k$, then these are also equivalent to:
	\begin{enumerate}[resume]
		\item
			$\stackx$ is isomorphic to the quotient stack of some affine scheme over $k$ by an action of an affine group scheme of finite type over $k$.
	\end{enumerate}
\end{theorem}

\begin{mrem}
	A more general statement for all quasi-compact and quasi-seperated stacks over the integers can be found in \cite{gross-resolution}.
\end{mrem}


\subsection{Deformation to the normal cone}
\subsubsection{For schemes}

\begin{construction}
	Let $Y$ be a scheme and $X$ a closed subscheme of $Y$.
	\begin{enumerate}
		\item
			Let $\shi$ be the ideal sheaf of $X$ inside $Y$. 
			The \emph{normal cone} $C_X$ of $X \hookrightarrow Y$ is defined via the relative spectrum over $X$ as
			\[
				C_X 
				\defined
				\spec_X\left( \bigoplus_{n\geq 0} \shi^n/\shi^{n+1}\right).
			\]
			The morphism $\bigoplus_{n \geq 0}\left( \shi^n/\shi^{n+1}\right) \to \ox$ which is the canonical isomorphism in degree zero and zero everywhere else determines a morphism $X \to C_X$ called the \emph{zero section embedding}.

	\end{enumerate}
\end{construction}
