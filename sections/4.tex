\section{Motivic homotopy theory of schemes}
\subsection{Tensor product of $\infty$-categories}

\begin{theorem}[\cite{ha}*{4.8.1.15}]
	Denote by $\mathrm{Pr^L}$ the $\infty$-category of presentable $\infty$-categories.
	\glsadd{presentable}
	Then $\mathrm{Pr^L}$ has a symmetric monoidal structure $\tensor$, which is uniquely characterized by the property that the bifunctor $-\tensor -$ preservers small colimits seperately in each variable.
\end{theorem}
\begin{prop}
	\leavevmode
	\begin{enumerate}
		\item
			Denote by $\ancat$ the $\infty$-category of anima.
			Then $\ancat$ is the unit in $(\prsntable,\tensor)$.
		\item
			Let $\ccat,\dcat$ be presentable $\infty$-categories.
			Then $\rfun(\dcat\op,\ccat)$ is presentable too.
			Moreover, $\ccat \tensor \dcat \cong \rfun(\dcat\op,\ccat)$ holds canonically.
		\item
			The symmetric monoidal $\infty$-category $(\prsntable,\tensor)$ is closed, with inner hom objects given by $\lfun(-,-)$.
	\end{enumerate}
\end{prop}

\begin{cor}
	The $\infty$-category $\ancat$ is an algebra object in $(\prsntable,\tensor)$.
\end{cor}

\subsection{Smashing localizations}
\begin{defn}
	Let $\ccat$ be an $\infty$-category.
	A \emph{Bousfield localization} of $\ccat$ is a functor $L \mc \ccat \to \dcat$ which admits a fully faithful right adjoint $R\mc \dcat \to \ccat$.
\end{defn}
\begin{notation}
	If $L \mc \ccat \leftrightarrows \dcat\mcc R$ is a Bousfield localization, then we will often identify $\dcat$ with the essential image $R(\dcat) \sse \ccat$ and call it the \emph{category of local objects (of $L$)}.
	We will also sometimes omit the functor $R$ from the notation, and write $L$ instead of $RL$.
\end{notation}

\begin{defn}
	Let $L \mc \ccat \leftrightarrows \dcat\mcc R$ be a Bousfield localization, where the $\infty$-category $\ccat$ has a symmetric monoidal structure $\tensor$.
	We say that the localization is \emph{smashing} if $RL = I\tensor (-)$ holds for some $I\in\ccat$.
\end{defn}

\begin{prop}
	Let $\ccat^{\tensor}$ be a symmetric monoidal $\infty$-category and $I \in \ccat$ an object.
	Then $I\tensor (-)$ is the left adjoint functor of a Bousfield localization if and only if $I$ is an idempotent object of $(\ccat,\tensor)$.
\end{prop}

\begin{theorem}
	Let  be a symmetric monoidal $\infty$-category and $L\mc \ccat \to \dcat$ a smashing localization.
	Then there is a unique symmetric monoidal structure $\dcat^{\tensor}$ on $\dcat$ such that $L$ extends to a symmetric monoidal functor $L \mc \ccat^{\tensor} \to \dcat^{\tensor}$.
	Moreover, $L$ induces a localization $L' \mc \calg(\ccat) \to \calg(\dcat)$, such that the diagram
	\[
		\begin{tikzcd}
			\calg(\ccat)
			\ar{r}[above]{L'}
			\ar{d}
			&
			\calg(\dcat)
			\ar{d}
			\\
			\ccat
			\ar{r}[below]{L}
			&
			\dcat
		\end{tikzcd}
		\]
		commutes.
		For every $A \in \calg(\ccat)$, there is a unique algebra structure on $RL(A)$, such that the unit map $A \to RL(A)$ extents to a map of commutative algebras.
\end{theorem}

\begin{example}
	As a particular instance where we want to apply this theorem is the case where  $\ccat = \ancat_{\ast}$ is the $\infty$-category of \emph{pointed anima}.
	Then $(\ancat_{\ast},S^0)$ is an idempotent object of $\prsntable$ (\cite{ha}*{4.8.2.11}).
	For every presentable $\ccat \in \prsntable$, there is a canonical isomorphism $\ccat \tensor \ancat_{\ast} \cong \ccat_{\ast}$, where $\ccat_{\ast} \defined \ccat_{\ast/}$ for the terminal object $\ast \in \ccat$ (recall that $\ccat$ being representable implies in particular that it has all small colimits).
	So we can identify the essential image of $(-)\tensor \ancat_{\ast}$ with the category $\pointedprsntable$ of \emph{pointed presentable $\infty$-categories}.
\end{example}

\subsection{Stabilization and spectra}

\subsection{Motives}
\begin{convention}
	\label{4:motives-z-assumption}
	In this subsection, we fix a noetherian scheme $Z$ of finite K***l dimension.
	We denote by $\smft/Z$ the nerve of the 1-category of smooth, seperable finite-type $Z$-schemes.
	The condition that $\dim(Z) < \infty$ is important to ensure that we are working with the ``correct notion of sheaves''.
\end{convention}
\subsubsection{Unstable Motivic spaces}

\begin{defn}
	Let $X$ be a scheme.
	A \emph{Nisnevich square} over $X$ is a cartesian diagram of $Z$-schemes
	\[
		\begin{tikzcd}
		W
		\ar{r}
		\ar{d}
		\ar[phantom]{rd}{\lrcorner}
		&
		V
		\ar{d}[right]{\pi}
		\\
		U
		\ar[hookrightarrow]{r}[below]{i}
		&
		X
		\end{tikzcd}
	\]
	where $i$ is an open immersion, $\pi$ is étale and it induces an isomorphism 
	\[
		V \times_X (X\setminus U)
		\isomorphism
		V \setminus W.
	\]
\end{defn}

\begin{defn}
	Let $Z$ be as in \cref{4:motives-z-assumption}. 
	\begin{enumerate}
		\item
			A presheaf $F\mc (\smft/Z)\op \to \ancat$ is \emph{$\affa^1$-invariant} if for every $X \in \smft/Z$, the projection $X \times \affa^1 \to X$ induces an equivalence $F(X) \isomorphism F(X \times \affa^1)$.
		\item 
			A presheaf $F \mc (\smft/Z)\op \to \ancat$ is \emph{Nisnevich local} if $F(\emptyset)$ is contractible, it sends finite coproducts in $\smft/Z$ to finite products in $\ancat$, and every Nisnevich square in $\smft/Z$ gets mapped to a cartesian square in $\ancat$:
			\[
				\begin{tikzcd}
				W
				\ar{r}
				\ar{d}
				\ar[phantom]{rd}{\lrcorner}
				&
				V
				\ar{d}
				\\
				U
				\ar[hookrightarrow]{r}
				&
				X
				\end{tikzcd}
				\overset{F}{\longmapsto}
				\begin{tikzcd}
				F(X)
				\ar{r}
				\ar{d}
				\ar[phantom]{rd}{\lrcorner}
				&
				F(U)
				\ar{d}
				\\
				F(V)
				\ar{r}
				&
				F(W)
				\end{tikzcd}
			\]
	\end{enumerate}

	We denote by $\motspaces(Z)$ the full subcategory of all presheaves $(F\mc \smft/Z)\op \to \ancat$ that are $\affa^1$-invariant and Nisnevich local.
	This category is called the \emph{category of (unstable) motivic spaces} (over $Z$).\glsadd{motivic-spaces}
\end{defn}

\begin{mrem}
	Denote by $L_{\affa^1}\presheaf(\smft/Z)$ the full subcategory of $\presheaf(\smft/Z)$ spanned by the $\affa^1$-invariant presheaves.
	They always satisfy Nisnevich descent, but are rarely $\affa^1$-invariant, even though all mapping spaces in $\smft/Z$ are discrete (so in particular contractible).
	The problem is that the equivalence $F(X) \isomorphism F(X\times \affa^1)$ has to be induced by the projection, which already fails for $\map(-,\affa^1)$.
\end{mrem}

\begin{mexample}
	One example of a representable sheaf of $\smft/Z$ that defines an unstable motivic space is the multiplicative group scheme $\gmult$, if $Z$ is seperated.
	Again, $\gmult$ automatically satisfies Nisnevich descent.
	To show that it is $\affa^1$-homotopy invariant, assume first that everything is affine (both the base scheme and the object we're evaluating at), say $Z = \spec(k)$ and $X = \spec(R)$.
	Then this boils down to showing that the map 
	\begin{align*}
		\hom(k[t,t^{-1},R)
		&
		\longrightarrow 
		\hom(k[t,t^{-1}],R[t])
		\\
		\left[k[t,t^{-1}]] \to  R\right]
		&
		\longmapsto 
	\left[k[t,t^{-1}]] \to R \hookrightarrow R[t]\right]
	\end{align*}
	is a bijection.
	Now every $k$-algebra morphism $k[t,t^{-1}] \to R$ is determined by its value on $t$, which must be a unit.
	Since $k$ is reduced and $k\to R$ is smooth, $R$ is reduced too, and so $(R[t])^{\times} = R^{\times}$ holds --- so every map $k[t,t^{-1}] \to R[t]$ is uniquely determined by its value on $R$.
\end{mexample}

\begin{mrem}
	Both the inclusion of the full subcategory of $\affa^1$-invariant pre\-shea\-ves and of Nisnevich presheaves admit left adjoints, which we denote by $L_{\affa^1}$ and $L_{\mathrm{Nis}}$ respectively.
	We think of them of taking $\affa^1$-invariant respectively Nisnevich-resolutions. \coms (make this more precise --- for nisnevich, this is some cofibrant replacement in a model category. i don't know for $\affa^1$ but it holds that 
	\[
		(L_{\affa^1}F)(X) \cong \colim_{\Delta\op} F(X \times \Delta^n),
	\]
	where $\Delta^n \sse \affa^n$ is the \emph{standard cosimplicial scheme} determined by the equation $T_0+\ldots+T_n = 1$.
	) \come
	We can obtain $\motspaces(Z)$ by alternatingly applying $L_{\affa^1}$ and $L_{\mathrm{Nis}}$.
	\coms
	Does that mean that 
	\[
		\motspaces(Z) \cong
		\colim
		\left(
		\begin{tikzcd}
		\presheaf(\smft/Z)
		\ar{r}[above]{L_{\affa^1}}
		&
		\presheaf(\smft/Z)
		\ar{r}[above]{L_{\mathrm{Nis}}}
		&
		\presheaf(\smft/Z)
		\ar{r}[above]{L_{\affa^1}}
		&
		\ldots
		\end{tikzcd}
		\right)
	\]
	holds in $\catinfty$?
	\come
\end{mrem}

\subsubsection{Symmetric monoidal structure on $\motspaces(Z)$}

\begin{mlem}
	Let $S^1 \defined \Delta^1/\partial \Delta^1$ be the \emph{simplicial circle} (pointed at the image of $\partial \Delta^1$).
	Then the suspension in $\motspaces(Z)_{\ast}$ coincides with the smash product $(-)\wedge S^1$.
\end{mlem}

\subsubsection{Stable motivic spaces}

\begin{mlem}
	Denote by $\projp^1$ the projective line.
	Then the following commutative diagram is a Nisnevich covering of $(\projp^1,1)$:
	\[
		\begin{tikzcd}
		\affa^1\setminus \lset 0 \rset
		\ar{r}
		\ar{d}
		&
		\affa^1
		\ar{d}
		\\
		\affa^1
		\ar{r}
		&
		\projp^1
		\end{tikzcd}
	\]
	Here we regard $\affa^1$ as pointed at 1, and the maps are given by $x \mapsto (x:1)$ respectively $x \mapsto (1:x)$.
	Furthermore, 
	\[
		(\projp^1,\infty)
		\cong 
		\gmult \wedge S^1
	\]
	holds in $H(Z)_{\ast}$.
\end{mlem}

\subsection{Functoriality}
\begin{construction}
	Let $\affcat$ be the category of affine schemes.
	We write $\affcat^{\dagger}$ for the full subcategory of affine schemes that are noetherian and of finite Krull dimension.
	Consider now the full subcategory $\scatfont{Sm}^{\dagger} \sse \arcat(\nerve(\affcat)\op)$ spanned by those morphisms $\spec(A) \to \spec(k)$ in $\nerve(\affcat)$ where $k$ is noetherian and of finite Krull dimension, and $k \to A$ is smooth and of finite type.
	Since both being smooth and being of finite presentation are stable under pullbacks, we get from \cref{3:arrowcat-co-cartesian} that the target functor 
	\[
		\scatfont{Sm}^{\dagger} \to \nerve(\affcat^{\dagger})\op
	\]
	is a cocartesian fibration, and so the Grothendieck construction provides us with a functor
	$
		F \mc 
		\nerve(\affcat^{\dagger})\op 
		\to 
		\catinfty,
	$
	which sends $\spec(k)$ to the nerve $\nerve(\scatfont{AffSm^{ft}}/k)$ (where $\scatfont{AffSm^{ft}}/k$ is the category of smooth finite type $k$-algebras).
	This functor can be extended to a functor 
	\[
		F^{\times}
		\mc 
		\nerve(\affcat^{\dagger})\op
		\to 
		\calg(\catinfty),
	\]
	where the monoidial structure on $\nerve(\affcat^{\dagger})$ is given by the cartesian.
	Via the monoidal Yoneda embedding, this extends to a functor $\presheaf(F)^{\times} \mc \nerve(\affcat^{\dagger})\op \to \calg(\presheaf(\prsntable))$.
	We are now left with localizing the $\infty$-category $\presheaf(\nerve(\scatfont{AffSm^{ft}}/k))$ in a ``functorial way'' at the Nisnevich- and $\affa^1$-homotopy equivalences to obtain the category $\motspaces(k)$ of unstable motivic spaces over $k$.
\end{construction}

	
